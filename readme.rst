# EDU NEXUS

## About


This is the git repository for our design lab project EDUNEXUS. EDUNEXUS is a web-based application that enables teachers to provide study materials for the NGO students without having to be physically present at the location. EDUNEXUS aims to set up a platform where several contributors can upload study materials for the students coupled with a discussion portal where the students may air their queries. This emancipates volunteers from maintaining burdensome teaching schedules and allows them to teach without being physically present.

## Technologies Used
* PHP - Backend Server.

* Framework- CodeIgniter supports MVC Architecture.

* HTML/CSS/Javascript - Frontend User Interaction.

* Database System - MySQL.

## Description Link : 

http://23.236.147.19/wiki/index.php?title=CodeStars:Main


